#define TrackFit_cxx
#include "TrackFit.h"

#include <iostream>

#include <TStyle.h>
#include <TCanvas.h>
#include <TMinuit.h>
#include <TMatrix.h>
#include <TMatrixD.h>
#include <TSystem.h>
#include <TNtuple.h>
#include <TH1.h>
#include <TLorentzVector.h>


const Int_t npts = 6; // chip nums
// 1 for chi2 fit and 2 for likelyhood fit

double xx[npts], yy[npts], zz[npts]; 
int plane[npts];
double sigmax=0.025/pow(12,0.5), sigmay=0.025/pow(12,0.5); 

void fcn_chisq(Int_t &npar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag)
{
  const Double_t a1 = par[0];
  const Double_t b1 = par[1];
  const Double_t a2 = par[2];
  const Double_t b2 = par[3];
  
  Double_t chisq=0.; 
  for( int i = 0; i < npts ; i++ ) {
    Double_t chi1  = ( a1*zz[i] + b1 - xx[i] )/sigmax; chi1 = chi1*chi1;   
    Double_t chi2  = ( a2*zz[i] + b2 - yy[i] )/sigmay; chi2 = chi2*chi2;   
    chisq += chi1 +chi2; 
  }	
  
  f = chisq;
  //std::cout << "chisq = " << chisq << std::endl;
}

// initialization and setup 

void Fit(Double_t results[])
{
  TMinuit *gMinuit = new TMinuit(4);  //initialize TMinuit with a maximum of 3 params
  gMinuit->SetFCN(fcn_chisq );
	
  Int_t    ierflg = 0;
  Double_t arglist[10], val[10], err[10];
	
  arglist[0] = -1;
  gMinuit->mnexcm("SET PRI", arglist, 1, ierflg);
  arglist[0] =0.5;
  gMinuit->mnexcm("SET ERR", arglist, 1, ierflg);
	
  // Set starting values and step sizes for parameters
  static Double_t vstart[4] = { 0.0010, 1.,  0.0010, 1.};
  static Double_t   step[4] = { 0.0001, 0.1,  0.0001, 0.1};

  gMinuit->mnparm(0, "a1", vstart[0], step[0], 0, 0, ierflg);
  gMinuit->mnparm(1, "b1", vstart[1], step[1], 0, 0, ierflg);
  gMinuit->mnparm(2, "a2", vstart[2], step[2], 0, 0, ierflg);
  gMinuit->mnparm(3, "b2", vstart[3], step[3], 0, 0, ierflg);

  // Now ready for minimization step
  arglist[0] = 5000;
  arglist[1] = 1.00;
  gMinuit->mnexcm("MIGRAD", arglist , 1, ierflg); // mimimization here ... 
  //gMinuit->mnexcm("MINOS" , arglist , 2, ierflg); // Minos error 
	
  // Print results
  Double_t amin,edm,errdef;
  Int_t nvpar,nparx,icstat;
  gMinuit->mnstat(amin,edm,errdef,nvpar,nparx,icstat);
  //std::cout<<"nparx="<<nparx<<std::endl;

  TString chnam;
  Double_t xlolim, xuplim;
  Int_t iuext, iuint;
	
  for(Int_t p = 0; p < 4; p++)
  { 
    results[p] =0.0; 
    gMinuit->mnpout(p, chnam, val[p], err[p], xlolim, xuplim, iuint);
    // printf("%2d %5s %8.2f +/- %8.2f\n", p, chnam.Data(), val[p]*1000, err[p]*1000); 
    // printf("%2d %5s %8.2f +/- %8.2f\n", p, chnam.Data(), val[p], err[p]);
    results[p] = val[p]; 
  }
  delete gMinuit; 
}

void TrackFit::Loop(TFile * f, TTree* myTree)
{  
   f->cd();
   double Chi2_Out, Chi2_OutY, Chi2_OutX;
   myTree->Branch("Chi2", &Chi2_Out);

   myTree->Branch("Chi2X", &Chi2_OutX);

   myTree->Branch("Chi2Y", &Chi2_OutY);

   vector<int>* vec_planeOut = new std::vector<int>();
   myTree->Branch("planeID", &vec_planeOut);

   vector<double>* vec_xlocOut = new std::vector<double>();
   myTree->Branch("xloc", &vec_xlocOut);

   vector<double>* vec_ylocOut = new std::vector<double>();
   myTree->Branch("yloc", &vec_ylocOut);
   TH1D * res[npts];
   TH1D * residual[npts][2];

   TString namea1 = "a1";
   TString nameb1 = "b1";
   TString namea2 = "a2";
   TString nameb2 = "b2";

   res[0] =  new TH1D(namea1, "", 100, -.001, .001);     
   res[2] =  new TH1D(namea2, "", 100, -.001, .001);
   res[1] =  new TH1D(nameb1, "", 100, -10, 10);
   res[3] =  new TH1D(nameb2, "", 100, -10, 10);

   for (int m=0; m < npts; m++){
     TString nameX = "residual_plane"+to_string(m)+"_x";
     TString nameY = "residual_plane"+to_string(m)+"_y";

     residual[m][0] = new TH1D(nameX, "", 100, -0.3, 0.3);
     residual[m][1] = new TH1D(nameY, "", 100, -0.12, 0.12);
   }

   Long64_t nentries = fChain->GetEntriesFast();
   double residualX;
   double residualY;
   Double_t results[4]; 

   std::vector<double> vec_chi2;
   std::vector<double> vec_chi2X;
   std::vector<double> vec_chi2Y;
   std::vector<double> vec_a1;
   std::vector<double> vec_a2;
   std::vector<double> vec_b1;
   std::vector<double> vec_b2;
   
   bool passRegion = false;

   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      fChain->GetEntry(jentry);   

      vec_planeOut->clear();
      vec_xlocOut->clear();
      vec_ylocOut->clear();  
     
      if(jentry%1000 == 0) std::cout<<"Processing event " <<jentry<<"..."<<std::endl;
      // if(jentry > 2) break;

      vec_chi2.clear();
      vec_chi2X.clear();
      vec_chi2Y.clear();
      vec_a1.clear();
      vec_b1.clear();
      vec_a2.clear();
      vec_b2.clear();

      for (int i = 0; i < TrackSize*npts; i+=npts)
      {
        double chi2 = 0;
        double chi2x = 0;
        double chi2y = 0;
        // std::cout << "i = " << i << std::endl;
        for (int j = 0; j < npts; j++)
        {
          // if (col->at(i+j) > 400 && col->at(i+j) < 750 && row->at(i+j) > 250 && row->at(i+j) < 450) 
          // { 
          //   passRegion = true;   
          // }
          // else
          // {
          //   passRegion = false;
          //   // std::cout << "aaaaaaabreak" << std::endl;
          //   break;
          // }
          passRegion = true;
          plane[j] = planeID->at(i+j);
	        xx[j] = xloc->at(i+j);
	        yy[j] = yloc->at(i+j);
	        if (j == 0) zz[j] = 20;
	        else if (j == 1) zz[j] = 60;
	        else if (j == 2) zz[j] = 100;
          else if (j == 3) zz[j] = 140;
	        else if (j == 4) zz[j] = 180;        
	        else if (j == 5) zz[j] = 200;
	        
        } 
        if (passRegion == true){
          Fit(results);
          for (int k = 0; k < npts; k++)
          {
            double dx = results[0]*zz[k] + results[1]-xx[k];
            double dy = results[2]*zz[k] + results[3]-yy[k];
	          chi2 += (dx * dx) / (sigmax*sigmax) + (dy * dy) / (sigmay*sigmay);
            chi2x += (dx * dx) / (sigmax*sigmax);
            chi2y += (dy * dy) / (sigmay*sigmay);
          }

          vec_chi2.push_back(chi2);
          vec_chi2X.push_back(chi2x);
          vec_chi2Y.push_back(chi2y);
          vec_a1.push_back(results[0]);
          vec_b1.push_back(results[1]);
          vec_a2.push_back(results[2]);
          vec_b2.push_back(results[3]);
        }
        else
        {
          // std::cout << "aaaaaaacontinue" << std::endl;
          continue;
        }
      }
      // std::cout << "aaaaaa" << std::endl;
      if (passRegion == true){
        // for (int i = 0; i < TrackSize; i++) std::cout << "vec_chi2 " << i << " = " << vec_chi2[i]<< std::endl;
        double minChi2 = *min_element(vec_chi2.begin(), vec_chi2.end());
        int minPos = min_element(vec_chi2.begin(), vec_chi2.end()) - vec_chi2.begin();

        double minChi2x = *min_element(vec_chi2X.begin(), vec_chi2X.end());
        int minPosx = min_element(vec_chi2X.begin(), vec_chi2X.end()) - vec_chi2X.begin();

        double minChi2y = *min_element(vec_chi2Y.begin(), vec_chi2Y.end());
        int minPosy = min_element(vec_chi2Y.begin(), vec_chi2Y.end()) - vec_chi2Y.begin();
     
        //  std::cout << "minChi2 = " << minChi2 << std::endl;
        //  std::cout << "minPos = " << minPos << std::endl;
        if (minPos != minPosx || minPos != minPosy) continue;
        if (minChi2 > 15000) continue; 
        Chi2_Out = minChi2;
        Chi2_OutX = minChi2x;
        Chi2_OutY = minChi2y;
        for (int l = 0; l < npts; l++)
        {
          vec_planeOut->push_back(planeID->at(minPos*npts + l));
          vec_xlocOut->push_back(xloc->at(minPos*npts + l));
          vec_ylocOut->push_back(yloc->at(minPos*npts + l));
       
          residualX = vec_a1[minPos]*zz[l] + vec_b1[minPos] - xloc->at(minPos*npts + l);
          residualY = vec_a2[minPos]*zz[l] + vec_b2[minPos] - yloc->at(minPos*npts + l);

          residual[l][0] -> Fill(residualX);
          residual[l][1] -> Fill(residualY);
        }
       myTree->Fill();
      }
   }
   myTree->Write();

   for (int j=0; j<npts; j++)
   {
      residual[j][0] -> Fit("gaus");
      residual[j][1] -> Fit("gaus");
      residual[j][0] -> Write();
      residual[j][1] -> Write();  
   }

    /*
    for (int i=0; i<4; i++){
      // std::cout<<"results"<<i<<":"<<results[i]<<std::endl;
      res[i] -> Fill(results[i]);
    }

    for (int i=0; i<4; i++){
      res[i] -> Fit("gaus", "Q");
      res[i] -> Write();
    }
   
   */  
   //delete residual, res;
   //f->Close(); 
}

int main()
{
  TFile * f = new TFile("Output/testRun179SelReSel6clusters_1hit.root", "RECREATE");
  TTree* myTree = new TTree("TrackInfo", "Selected tracks with minimum Chi2");

  TrackFit * tf;
  tf= new TrackFit();
     
  tf->Loop(f, myTree);
  f->Close();
  delete tf; 
}
