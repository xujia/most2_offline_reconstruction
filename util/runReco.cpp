#include<iostream>
#include<fstream>
#include<vector>

#include "FindTimeStamp.h"
#include "FindAllHits.h"
#include "CreateCluster.h"
#include "CombineAllTracks.h"
#include "constants.h"

int main(int argc, char **argv)
{

    // DeBinary* decode;
    // for (int i = 1; i < 2; i++)
    // // for (int i = 1; i < NumOfChips + 1; i++)
    // {
    //   TString Chip = "0" + std::to_string(i);
    //   decode = new DeBinary();
    //   std::cout << "======== Decoding " << Chip << "th " << "chip file =======" << std::endl;
    //   decode->DoReading(RunNumber, Chip);
    //   std::cout << "================= Done =================" << std::endl; 
      
    // }    
    // delete decode;

    FindTimeStamp* findTimeStamp = new FindTimeStamp();
    findTimeStamp->initialize();
    std::vector<std::vector<int>> Turnpoint = findTimeStamp->FindTurnPoint();
    findTimeStamp->FindTime(Turnpoint);
    delete findTimeStamp;

    FindAllHits* findHits = new FindAllHits();
    findHits->FindHits(Turnpoint);
    delete findHits;
  
    CreateCluster* clustering = new CreateCluster();
    clustering->FormCluster();
    delete clustering;

    CombineAllTracks* combinetracks = new CombineAllTracks();
    combinetracks->CombineCluster();
    delete combinetracks;



}
