#include "IncludeFun.h"
#include "constants.h"
#include "FindTimeStamp.h"

#include <iostream>
#include <fstream>
#include <vector>

// Get the same FPGAtimeStamp
FindTimeStamp::FindTimeStamp()
{
  std::cout << "**********************************" << std::endl;
  std::cout << "*** finding FPGA time matched ***"  << std::endl;
  std::cout << "**********************************" << std::endl;
}

FindTimeStamp::~FindTimeStamp()
{

}

bool FindTimeStamp::initialize()
{
  file1 = new TFile(directory+RunNumber+"/OutputhitSel01.root", "read"); 
  file2 = new TFile(directory+RunNumber+"/OutputhitSel02.root", "read");
  file3 = new TFile(directory+RunNumber+"/OutputhitSel03.root", "read");
  file4 = new TFile(directory+RunNumber+"/OutputhitSel04.root", "read");
  file5 = new TFile(directory+RunNumber+"/OutputhitSel05.root", "read");
  file6 = new TFile(directory+RunNumber+"/OutputhitSel06.root", "read");

  return true;
}

std::vector<std::vector<int>> FindTimeStamp::FindTurnPoint()
{
  std::vector<std::vector<int>> vec_Findpos;
  std::vector<int> vec_Findpos1 = FindZero(file1);
  std::vector<int> vec_Findpos2 = FindZero(file2);
  std::vector<int> vec_Findpos3 = FindZero(file3);
  std::vector<int> vec_Findpos4 = FindZero(file4);
  std::vector<int> vec_Findpos5 = FindZero(file5);
  std::vector<int> vec_Findpos6 = FindZero(file6);

  std::cout << vec_Findpos1.size() << std::endl;
  std::cout << vec_Findpos2.size() << std::endl;
  std::cout << vec_Findpos3.size() << std::endl;
  std::cout << vec_Findpos4.size() << std::endl;
  std::cout << vec_Findpos5.size() << std::endl;
  std::cout << vec_Findpos6.size() << std::endl;

  for (int j = 0; j < vec_Findpos4.size(); j++)
  {
    std::cout << vec_Findpos1[j] << std::endl;
    std::cout << vec_Findpos2[j] << std::endl;
    std::cout << vec_Findpos3[j] << std::endl;
    std::cout << vec_Findpos4[j] << std::endl;
    std::cout << vec_Findpos5[j] << std::endl;
    std::cout << vec_Findpos6[j] << std::endl;
    std::cout << "=======" << std::endl;
  }
  vec_Findpos.push_back(vec_Findpos1);
  vec_Findpos.push_back(vec_Findpos2);
  vec_Findpos.push_back(vec_Findpos3);
  vec_Findpos.push_back(vec_Findpos4);
  vec_Findpos.push_back(vec_Findpos5);
  vec_Findpos.push_back(vec_Findpos6);

  return vec_Findpos;
}


void FindTimeStamp::FindTime(std::vector<std::vector<int>> vec_Findpos){

    TFile *OutFile = new TFile(directory+RunNumber+"/SameTimeLooseFirst4TesttSel.root", "RECREATE");
    TTree* myTree = new TTree("TimeInfo", "beam data");

    TTree *t1 = (TTree*)file5->Get("HitsInfo");   // Chip04
    TTree *t2 = (TTree*)file4->Get("HitsInfo");   // Chip03
    TTree *t3 = (TTree*)file3->Get("HitsInfo");   // Chip02
    TTree *t4 = (TTree*)file2->Get("HitsInfo");   // Chip01

    std::vector<int>* vec_pos = new std::vector<int>();
    myTree->Branch("pointor", &vec_pos);
    std::vector<int>* vec_timeFPGA = new std::vector<int>();
    myTree->Branch("timeFPGA", &vec_timeFPGA);
    std::vector<int>* vec_timeChip = new std::vector<int>();
    myTree->Branch("timeChip", &vec_timeChip);
    std::vector<int>* vec_row      = new std::vector<int>();
    myTree->Branch("row", &vec_row);
    std::vector<int>* vec_col      = new std::vector<int>();
    myTree->Branch("col", &vec_col);
    std::vector<int>* vec_plane      = new std::vector<int>();
    myTree->Branch("planeID", &vec_plane);

    int row1, row2, row3, row4, col1, col2, col3, col4, timeFPGA1, timeFPGA2, timeFPGA3, timeFPGA4, timeChip1, timeChip2, timeChip3, timeChip4;

    t1->SetBranchAddress("row", &row1);
    t1->SetBranchAddress("col", &col1);
    t1->SetBranchAddress("timeFPGA", &timeFPGA1);
    t1->SetBranchAddress("timeChip", &timeChip1);

    t2->SetBranchAddress("row", &row2);
    t2->SetBranchAddress("col", &col2);
    t2->SetBranchAddress("timeFPGA", &timeFPGA2);
    t2->SetBranchAddress("timeChip", &timeChip2);

    t3->SetBranchAddress("row", &row3);
    t3->SetBranchAddress("col", &col3);
    t3->SetBranchAddress("timeFPGA", &timeFPGA3);
    t3->SetBranchAddress("timeChip", &timeChip3);

    t4->SetBranchAddress("row", &row4);
    t4->SetBranchAddress("col", &col4);
    t4->SetBranchAddress("timeFPGA", &timeFPGA4);
    t4->SetBranchAddress("timeChip", &timeChip4);

    int TotHitsNum4 = (int)t4->GetEntries();
    int TotHitsNum3 = (int)t3->GetEntries();
    int TotHitsNum2 = (int)t2->GetEntries();
    int TotHitsNum1 = (int)t1->GetEntries();

    std::cout << "TotHitsNum of chip1 = " << TotHitsNum4 << std::endl;
    std::cout << "TotHitsNum of chip2 = " << TotHitsNum3 << std::endl;
    std::cout << "TotHitsNum of chip3 = " << TotHitsNum2 << std::endl;
    std::cout << "TotHitsNum of chip4 = " << TotHitsNum1 << std::endl;

    int ltag = 0;
    int ktag = 0;
    int jtag = 0;
    
    int preFPGA = 0;
    for (int i = 0; i < (int)t1->GetEntries(); i++)
    {
      t1->GetEntry(i);
      if (i%1000 == 0) std::cout << "i = " << i << std::endl;
      // if (i > 1000000) break;
      int Items = findItems(&vec_Findpos[3], i);
      if ( Items >= 0)
      {
        std::cout << "found...." << std::endl;
        jtag = vec_Findpos[2][Items];   // 3th chip
        ktag = vec_Findpos[1][Items];   // 2th chip
        ltag = vec_Findpos[0][Items];   // 1th chip
      }
      // if (i > 10000000) break;
      for (int j = jtag; j < (int)t2->GetEntries(); j++)
      {
        t2->GetEntry(j);
        if (passTime(timeFPGA1, timeFPGA2, 1))
        {
          jtag = j + 1;
          for (int k = ktag; k < (int)t3->GetEntries(); k++)
          {
            t3->GetEntry(k);
            if (passTime(timeFPGA2, timeFPGA3, 1))
            {
              ktag = k + 1;
              vec_timeFPGA->clear();
              vec_timeChip->clear();
              vec_row->clear();
              vec_col->clear();
              vec_plane->clear();
              vec_pos->clear();

              int SavetimeFPGA1Times = 0;
              for (int l = ltag; l < (int)t4->GetEntries(); l++)
              {
                t4->GetEntry(l);
                if (passTime(timeFPGA3, timeFPGA4, 1))
                {
                  ltag = l + 1;
                  // std::cout << "passTime(timeFPGA3, timeFPGA4)" << std::endl;
                  if ( SavetimeFPGA1Times == 0)
                  {
                    vec_timeFPGA->push_back(timeFPGA1);
                    vec_timeChip->push_back(timeChip1);
                    vec_row->push_back(row1);
                    vec_col->push_back(col1);
                    vec_plane->push_back(3);
                    vec_pos->push_back(i);

                    vec_timeFPGA->push_back(timeFPGA2);
                    vec_timeChip->push_back(timeChip2);
                    vec_row->push_back(row2);
                    vec_col->push_back(col2);
                    vec_plane->push_back(2);
                    vec_pos->push_back(j);

                    vec_timeFPGA->push_back(timeFPGA3);
                    vec_timeChip->push_back(timeChip3);
                    vec_row->push_back(row3);
                    vec_col->push_back(col3);
                    vec_plane->push_back(1);
                    vec_pos->push_back(k);
                    SavetimeFPGA1Times++;
                  }

                  vec_timeFPGA->push_back(timeFPGA4);
                  vec_timeChip->push_back(timeChip4);
                  vec_row->push_back(row4);
                  vec_col->push_back(col4);
                  vec_plane->push_back(0);
                  vec_pos->push_back(l);
                }
                else if (passbreak(timeFPGA3, timeFPGA4, 1)) { ltag = l; break;}
                else if (passcontinue(timeFPGA3, timeFPGA4, 1)) continue;
              }
              if(vec_timeFPGA->size() > 0)  myTree->Fill();
            }
            else if (passbreak(timeFPGA2, timeFPGA3, 1)) { ktag = k; break;}
            else if (passcontinue(timeFPGA2, timeFPGA3, 1)) { continue; }
          }
        }
        else if (passbreak(timeFPGA1, timeFPGA2, 1)) { jtag = j; break;}
        else if (passcontinue(timeFPGA1, timeFPGA2, 1)) continue;
      }
    }
  myTree->Write();
  OutFile->Close();
  
}
