#include <TFile.h>

#include "IncludeFun.h"
#include "island1.h"
#include "constants.h"
#include "FindAllHits.h"

// Get all hits with the same FPGA timeStamp

FindAllHits::FindAllHits()
{
  std::cout << "********************************************" << std::endl;
  std::cout << "*** finding all hits with same FPGA time ***"  << std::endl;
  std::cout << "********************************************" << std::endl;
}

FindAllHits::~FindAllHits()
{

}


void FindAllHits::FindHits(std::vector<std::vector<int>> vec_Findpos){

    TFile *Orifile1 = new TFile(directory+RunNumber+"/OutputhitSel01.root", "read"); // first plane   
    TFile *Orifile2 = new TFile(directory+RunNumber+"/OutputhitSel02.root", "read");    
    TFile *Orifile3 = new TFile(directory+RunNumber+"/OutputhitSel03.root", "read");    
    TFile *Orifile4 = new TFile(directory+RunNumber+"/OutputhitSel04.root", "read");    
    TFile *Orifile5 = new TFile(directory+RunNumber+"/OutputhitSel05.root", "read");
    TFile *Orifile6 = new TFile(directory+RunNumber+"/OutputhitSel06.root", "read"); 


    std::vector<TFile*> AllInputFiles;
    AllInputFiles.push_back(Orifile1);
    AllInputFiles.push_back(Orifile2);
    AllInputFiles.push_back(Orifile3);
    AllInputFiles.push_back(Orifile4);
    AllInputFiles.push_back(Orifile5);
    AllInputFiles.push_back(Orifile6);

    TFile *InFile = new TFile(directory+RunNumber+"/SameTimeLooseFirst4TesttSel.root", "read");
    TTree *tInFile = (TTree*)InFile->Get("TimeInfo");

    TFile *OutFile = new TFile(directory+RunNumber+"/AllSameTimeHitsLooseFirst4Testt333Sel.root", "RECREATE");

    // Out Tree
    TTree* myTree = new TTree("TimeInfo", "beam data");
    vector<int>* vec_timeFPGAOut = new std::vector<int>();
    myTree->Branch("timeFPGA", &vec_timeFPGAOut);

    vector<int>* vec_timeChipOut = new std::vector<int>();
    myTree->Branch("timeChip", &vec_timeChipOut);

    vector<int>* vec_planeOut    = new std::vector<int>();
    myTree->Branch("planeID", &vec_planeOut);

    vector<int>* vec_rowOut      = new std::vector<int>();
    myTree->Branch("row", &vec_rowOut);

    vector<int>* vec_colOut      = new std::vector<int>();
    myTree->Branch("col", &vec_colOut);

    vector<int>* vec_clusterOut    = new std::vector<int>();
    myTree->Branch("clusterID", &vec_clusterOut);

    vector<int>* vec_clusterNum    = new std::vector<int>();
    myTree->Branch("clusterNum", &vec_clusterNum);

    vector<vector<int>*> vec_BranchOut;

    vector<int>* vec_pos = new std::vector<int>();
    tInFile->SetBranchAddress("pointor", &vec_pos);
    vector<int>* vec_timeFPGA = new std::vector<int>();
    tInFile->SetBranchAddress("timeFPGA", &vec_timeFPGA);
    vector<int>* vec_timeChip = new std::vector<int>();
    tInFile->SetBranchAddress("timeChip", &vec_timeChip);
    vector<int>* vec_row      = new std::vector<int>();
    tInFile->SetBranchAddress("row", &vec_row);
    vector<int>* vec_col      = new std::vector<int>();
    tInFile->SetBranchAddress("col", &vec_col);
    vector<int>* vec_plane      = new std::vector<int>();
    tInFile->SetBranchAddress("planeID", &vec_plane);

    int preFPGA = 0;
    int curFPGA = 0;
    int tag[NumOfChips]; for (int i = 0; i < NumOfChips; i++) tag[i] = 0;
    int ith = 0;

    int TotalHits;
    vector<double> Selrow; vector<double> Selcol;
    for (int m = 0; m < (int)tInFile->GetEntries(); m++)
    {
      tInFile->GetEntry(m);
      int target = vec_timeFPGA->at(0);
      preFPGA = curFPGA;
      curFPGA = target;
      if (preFPGA > curFPGA){
        std::cout << "ith = " << ith << std::endl;
        std::cout << "preFPGA = " << preFPGA << std::endl;
        std::cout << "timeFPGA1 = " << target << std::endl;

        tag[0] = vec_Findpos[0][ith];
        tag[1] = vec_Findpos[1][ith];
        tag[2] = vec_Findpos[2][ith];
        tag[3] = vec_Findpos[3][ith];
        tag[4] = vec_Findpos[4][ith];
        tag[5] = vec_Findpos[5][ith];
        ith++;
        std::cout << "****************" << std::endl;
      }

      vec_timeFPGAOut->clear();
      vec_timeChipOut->clear();
      vec_rowOut->clear();
      vec_colOut->clear();
      vec_planeOut->clear();
      vec_clusterOut->clear();
      vec_clusterNum->clear();

      vec_BranchOut.clear();

      vec_BranchOut.push_back(vec_planeOut);
      vec_BranchOut.push_back(vec_timeFPGAOut);
      vec_BranchOut.push_back(vec_timeChipOut);
      vec_BranchOut.push_back(vec_rowOut);
      vec_BranchOut.push_back(vec_colOut);

      if (m%10000 == 0) std::cout << "processing " << m << "events" <<std::endl;

      int FindTotalHits[NumOfChips]; for (int i = 0; i < NumOfChips; i++) FindTotalHits[i] = 0;
      for (int i = 0; i < NumOfChips; i++)
      {
        FindTotalHits[i] = FindTime(AllInputFiles[i], target, vec_BranchOut, i, tag);
      }
 
      if (FindTotalHits[0] > 0 && FindTotalHits[1] > 0 && FindTotalHits[2] > 0 && FindTotalHits[3] > 0 && FindTotalHits[4] > 0 && FindTotalHits[5] > 0)
      {
        TotalHits = vec_timeFPGAOut->size();

        int aa[TotalHits]; for (int i = 0; i < TotalHits; i++) aa[i] = 0;
        int accuPlaneHits = 0;
        for (int i = 0; i < NumOfChips; i++)
        {
          Selrow.clear();
          Selcol.clear();
          std::vector<int> Selplane = findVectors(vec_planeOut, i);
          for (int j = Selplane[0]; j < Selplane[Selplane.size() - 1] + 1; j++)
          {
            Selrow.push_back(vec_rowOut->at(j));
            Selcol.push_back(vec_colOut->at(j));
          }
          double size = 0.025;
          for(auto& tx:Selrow){ tx*= size; }
          for(auto& ty:Selcol){ ty*= size; }

          island *t_island = new island(Selrow, Selcol);
          unordered_map<int,vector<pair<double, double>>> islands = t_island->getislands();
          unordered_map<int,vector<int>> islands_id = t_island->getislands_id();

          for(int i = 0; i<t_island->getnumOfislands();i++){
            //cout<<"cluster "<<i<<endl;
            for(int j=0;j<islands[i].size();j++){
              //cout<<"(x,y):"<<int(islands[i][j].first*40+EPSINON)<<","<<int(islands[i][j].second*40+EPSINON)<<" id:"<<islands_id[i][j]<<endl;
              int index = islands_id[i][j];
              //std::cout<<"islands_id[i][j] = " << index << std::endl; //
              //std::cout << "i = " << i << std::endl;
              aa[accuPlaneHits+index] = i;
              vec_clusterNum->push_back(t_island->getnumOfislands());
            }
            // cout<<endl;
          }
          //std::cout << "==================" << std::endl;
          delete t_island;
          //std::cout << "Selrow.size() = " << Selrow.size() << std::endl;
          accuPlaneHits = accuPlaneHits + Selrow.size();
        }
        for (int i = 0; i < TotalHits; i++)
        {
          //std::cout << "aa[i] = " << aa[i] << std::endl;
          vec_clusterOut->push_back(aa[i]);
        }  
        myTree->Fill();
      }
    }
  myTree->Write();
  InFile->Close();
  OutFile->Close();
}