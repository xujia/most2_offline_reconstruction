#include <iostream>
#include <vector>
#include <algorithm>

#include "IncludeFun.h"
#include "CombineAllTracks.h"

using namespace std;

CombineAllTracks::CombineAllTracks()
{
  std::cout << "************************************" << std::endl;
  std::cout << "*** combining all possible tracks***"  << std::endl;
  std::cout << "************************************" << std::endl;
}

CombineAllTracks::~CombineAllTracks()
{

}

void CombineAllTracks::CombineCluster(){

    TFile *InFile = new TFile(directory+RunNumber+"/Cluster5xySel6.root", "read");
    TTree *tInFile = (TTree*)InFile->Get("Event");

    int RunNum;
    tInFile->SetBranchAddress("RunNumber", &RunNum);
    int TotalClusters;
    tInFile->SetBranchAddress("TotClusterNum", &TotalClusters);
    vector<int>* vec_plane           = new std::vector<int>();
    tInFile->SetBranchAddress("planeID", &vec_plane);
    vector<int>* vec_ClusterNum      = new std::vector<int>();
    tInFile->SetBranchAddress("clusterNum", &vec_ClusterNum);
    vector<double>* vec_col          = new std::vector<double>();
    tInFile->SetBranchAddress("col", &vec_col);
    vector<double>* vec_row          = new std::vector<double>();
    tInFile->SetBranchAddress("row", &vec_row);
    vector<int>* vec_hitsNum         = new std::vector<int>();
    tInFile->SetBranchAddress("hitsNum", &vec_hitsNum);
    vector<double>* vec_xloc         = new std::vector<double>();
    tInFile->SetBranchAddress("xloc", &vec_xloc);
    vector<double>* vec_yloc         = new std::vector<double>();
    tInFile->SetBranchAddress("yloc", &vec_yloc);

    TFile *OutFile = new TFile(directory+RunNumber+"/"+RunNumber+"_AllTrackxySel6.root", "RECREATE");
    // Out Tree
    TTree* myTree = new TTree("TrackInfo", "Roughly selected Tracks");

    int RunNumOut;
    myTree->Branch("RunNumber", &RunNumOut);

    int TrackSize;
    myTree->Branch("TrackSize", &TrackSize);

    vector<int>* vec_planeOut        = new std::vector<int>();
    myTree->Branch("planeID", &vec_planeOut);
    vector<double>* vec_colOut       = new std::vector<double>();
    myTree->Branch("col", &vec_colOut);
    vector<double>* vec_rowOut       = new std::vector<double>();
    myTree->Branch("row", &vec_rowOut);
    vector<double>* vec_xlocOut      = new std::vector<double>();
    myTree->Branch("xloc", &vec_xlocOut);
    vector<double>* vec_ylocOut      = new std::vector<double>();
    myTree->Branch("yloc", &vec_ylocOut);

    int first_dimension;
    for (int m = 0; m < (int)tInFile->GetEntries(); m++){
      tInFile->GetEntry(m);
      if (m%10000 == 0) std::cout << "Processing " << m << "..." << std::endl;
      if (m > 530000) break;
      RunNumOut = RunNum;
      vec_rowOut->clear();
      vec_colOut->clear();
      vec_planeOut->clear();
      vec_xlocOut->clear();
      vec_ylocOut->clear();

      first_dimension = 1;
      vector<vector<int>> Selplane;
      for (int i = 0; i < NumOfChips; i++)
      {
        Selplane.push_back(findVectors(vec_plane, i));
        first_dimension *= Selplane[i].size();
      }

      TrackSize = first_dimension;
      
      int result[first_dimension][6];
      get_all_combination(Selplane, result);
      
      for (int i = 0; i < first_dimension; i++)
      {
        for (int j = 0; j < NumOfChips; j++)
        {
          // cout<<result[i][j]<<"\t";
          int Selposition = result[i][j];
          vec_planeOut->push_back(j);
          vec_rowOut->push_back(vec_row->at(Selposition));
          vec_colOut->push_back(vec_col->at(Selposition));
          vec_xlocOut->push_back(vec_xloc->at(Selposition));
          vec_ylocOut->push_back(vec_yloc->at(Selposition));
        }
        // cout<<endl;
      }
      // std::cout << "first_dimension = " << first_dimension << std::endl;
      myTree->Fill();
  }
  myTree->Write();
  InFile->Close();
  OutFile->Close();
}