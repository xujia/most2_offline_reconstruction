#include "IncludeFun.h"
#include "constants.h"
#include "CreateCluster.h"

CreateCluster::CreateCluster()
{
  std::cout << "********************************************" << std::endl;
  std::cout << "*** Calculation cluster position from all hits ***"  << std::endl;
  std::cout << "********************************************" << std::endl;
}

CreateCluster::~CreateCluster()
{

}


void CreateCluster::FormCluster(){
   
    TFile *InFile = new TFile(directory+RunNumber+"/AllSameTimeHitsLooseFirst4Testt333Sel.root", "read");
    TTree *tInFile = (TTree*)InFile->Get("TimeInfo");

    std::vector<int>* vec_timeFPGA = new std::vector<int>();
    tInFile->SetBranchAddress("timeFPGA", &vec_timeFPGA);
    std::vector<int>* vec_timeChip = new std::vector<int>();
    tInFile->SetBranchAddress("timeChip", &vec_timeChip);
    std::vector<int>* vec_row      = new std::vector<int>();
    tInFile->SetBranchAddress("row", &vec_row);
    std::vector<int>* vec_col      = new std::vector<int>();
    tInFile->SetBranchAddress("col", &vec_col);
    std::vector<int>* vec_plane      = new std::vector<int>();
    tInFile->SetBranchAddress("planeID", &vec_plane);
    std::vector<int>* vec_clusterID      = new std::vector<int>();
    tInFile->SetBranchAddress("clusterID", &vec_clusterID);
    std::vector<int>* vec_clusterNum     = new std::vector<int>();
    tInFile->SetBranchAddress("clusterNum", &vec_clusterNum);

    TFile *OutFile = new TFile(directory+RunNumber+"/Cluster5xySel6.root", "RECREATE");
    // Out Tree
    TTree* myTree = new TTree("Event", "beam data");

    int RunNum;
    myTree->Branch("RunNumber", &RunNum);
    int TotalClusters;
    myTree->Branch("TotClusterNum", &TotalClusters);
    std::vector<int>* vec_planeOut      = new std::vector<int>();
    myTree->Branch("planeID", &vec_planeOut);
    std::vector<int>* vec_ClusterNumOut      = new std::vector<int>();
    myTree->Branch("clusterNum", &vec_ClusterNumOut);
    std::vector<double>* vec_colOut      = new std::vector<double>();
    myTree->Branch("col", &vec_colOut);
    std::vector<double>* vec_rowOut      = new std::vector<double>();
    myTree->Branch("row", &vec_rowOut);
    std::vector<int>* vec_hitsNum      = new std::vector<int>();
    myTree->Branch("hitsNum", &vec_hitsNum);
    std::vector<double>* vec_xloc = new std::vector<double>();
    myTree->Branch("xloc", &vec_xloc);
    std::vector<double>* vec_yloc = new std::vector<double>();
    myTree->Branch("yloc", &vec_yloc);

    double aveCol, aveRow;
    double x, y;
    std::vector<int> SelCluID;
    for (int m = 0; m < (int)tInFile->GetEntries(); m++){
      //if (m%100 == 0) std::cout << "Processing " << m << "..." << std::endl;//if (m > 0) break;
      RunNum = 131;
      vec_rowOut->clear();
      vec_colOut->clear();
      vec_planeOut->clear();
      vec_ClusterNumOut->clear();
      vec_xloc->clear();
      vec_yloc->clear();
      vec_hitsNum->clear();

      tInFile->GetEntry(m);
      int accuPlaneHits = 0;
      TotalClusters = 0;
      for (int i = 0; i < NumOfChips; i++)
      {
        SelCluID.clear();
        std::vector<int> Selplane = findVectors(vec_plane, i);
        for (int j = Selplane[0]; j < Selplane[Selplane.size() - 1] + 1; j++)
        {
          SelCluID.push_back(vec_clusterID->at(j));
        }
        // for (int e = 0; e < SelCluID.size(); e++) std::cout << SelCluID[e] << std::endl;
        int planeClusters = vec_clusterNum->at(Selplane[0]);
        TotalClusters += planeClusters;
        for (int k = 0; k < planeClusters; k++)
        {
          aveRow = 0;
          aveCol = 0;
          std::vector<int> SelHitsInCluster = findVectors(&SelCluID, k);
          //if ((planeClusters > 5) && (SelHitsInCluster.size() == 1)) continue;
          for (int l = 0; l < SelHitsInCluster.size(); l++)
          {
            aveRow += vec_row->at(accuPlaneHits+SelHitsInCluster.at(l))/SelHitsInCluster.size();
            aveCol += vec_col->at(accuPlaneHits+SelHitsInCluster.at(l))/SelHitsInCluster.size();
          }
          // if (SelHitsInCluster.size() > 1) continue;
          vec_hitsNum->push_back(SelHitsInCluster.size());
          double  *PointXY = new double [2];
          pixelToPoint(aveRow, aveCol, PointXY);
          x = PointXY[0];
          y = PointXY[1];
          delete[] PointXY;

          vec_planeOut->push_back(i);
          vec_ClusterNumOut->push_back(planeClusters);

          

          vec_colOut->push_back(aveCol);
          vec_rowOut->push_back(aveRow);
          vec_xloc->push_back(x*(1e-3));
          vec_yloc->push_back(y*(1e-3));
        }
        accuPlaneHits = accuPlaneHits + SelCluID.size();
      }
    if (TotalClusters == 6) myTree->Fill();
    // if (vec_planeOut->size() == 6 && TotalClusters == 6) myTree->Fill();
    // myTree->Fill();
    }
  myTree->Write();
  InFile->Close();
  OutFile->Close();
}
