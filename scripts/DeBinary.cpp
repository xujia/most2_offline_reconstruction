#include<iostream>
#include<fstream>
#include<vector>
using namespace std;


void DoReading(TString RunNumber, TString Chip)
{
  TString directory = "/home/dell/MOST2_Offline_Reconstruction/DESY_TbData/";
  TFile* OutputFile = new TFile(directory+RunNumber+"/OutputhitSel"+Chip+".root", "RECREATE");
  // TFile* OutputFile = new TFile("/home/dell/MOST2_Offline_Reconstruction/MOST2_BSRF_Data/"+RunNumber+"/Outputhit"+Chip+".root", "RECREATE");
  TTree* myTree = new TTree("HitsInfo", "test beam data");

  int valid     = -1;
  int begFlag   = -1;
  int chipID    = 0;
  int dataCount = 0;
  int curID     = 0;

  int preID     = 0;
  int passVal   = 0;
  int row       = 0;
  double newrow = 0;
  double newcol = 0;
  int col       = 0;
  int timeFPGA  = 0;
  int timeChip  = 0;

  int prerow       = 0;
  int precol       = 0;
  int pretimeFPGA  = 0;
  int pretimeChip  = 0;

  int hitNumber = 0;  // in every packet
  bool IsTrailer = false;
  int num = 0;

  //myTree->Branch("begFlag", &begFlag);
  myTree->Branch("PacketID", &curID);
  myTree->Branch("PacketDataNum", &dataCount);
  myTree->Branch("HitsNum", &hitNumber);

  myTree->Branch("valid", &valid);
  myTree->Branch("chipID", &chipID);
  myTree->Branch("timeFPGA", &timeFPGA);
  myTree->Branch("timeChip", &timeChip);
  myTree->Branch("row", &row);
  myTree->Branch("col", &col);



  size_t length;
  ifstream inF;
  
  inF.open(directory+RunNumber+"/"+RunNumber+"-RunData-Board"+Chip+"/"+"Board-"+Chip+".bin", std::ifstream::binary);
  inF.seekg(0, ios::end);
  length = inF.tellg();
  cout << "the length of the file is " << length << " " << "byte" << endl;
  
  length = 8;
  unsigned char* data = new unsigned char[length]();

  inF.clear();
  inF.seekg(7, ios::beg);
  cout << "the position of the beginner " << inF.tellg() << endl;
  inF.get();
  if (!inF.eof()) {
    cout << "target reading..." << endl;
    while(inF.read(reinterpret_cast<char*>(data), sizeof(char)* length))
    {
      num++;
      if (num%500000 == 0) std::cout << "processing " << num << " packet..." << std::endl; 
      unsigned long long int* data0 = new unsigned long long int[length];
      // for (size_t i = 0; i < length; i++){
      //   data0[i] = data[i];
      //   cout << setw(2) << setfill('0') << setiosflags(ios::uppercase) << hex << data0[i]<<" ";
      // }
      // cout << "" << endl;
      // cout << "the position of the current pointor " << dec <<inF.tellg() << endl;
      if (data[0] == 0xaa && data[1] == 0xbb){
        hitNumber = 0;
        // cout << dec << "Reading one packet..." << endl;
        dataCount = data[2] & 0xff;
        preID = curID;
        curID = (data[4] << 24 & 0xffffffff) | (data[5] << 16 & 0xffffff) | (data[6] << 8 & 0xffff) | (data[7] & 0xff);
        int testID = preID + 1;
        if (curID - preID != 1) cout << "packet header ID error, Current ID should be " << dec << testID << ", rather than " << curID << endl;
        while (inF.read(reinterpret_cast<char*>(data), sizeof(char)* length))
        {
          precol = col;
          prerow = row;
          pretimeChip = timeChip;
          pretimeFPGA = timeFPGA;

          valid  = data[7] & 0x01;
          chipID = data[0] >> 4 & 0x0f;
          timeFPGA = (data[1] << 24 & 0xfffffff) | (data[2] << 16 & 0xffffff) | (data[3] << 8 & 0xffff) | (data[4] & 0xff);
          timeChip = (data[0] << 4 & 0xf0) | (data[1] >> 4 & 0x0f);
          row      = (data[6] << 3 & 0x3ff) | (data[7] >>5 & 0x07);
          col      = (data[5] << 1 & 0x1ff) | (data[6] >> 7 & 0x01);
          
          newrow = row/2;
          if ((row%4 == 0) || (row%4 == 3)) { newcol = col*2;}
          else {newcol = col*2 + 1;}
          if (newcol < 350 || newcol > 850  || newrow < 50 || newrow > 450) continue;
          if (precol == col && prerow == row && pretimeChip == timeChip && pretimeFPGA == timeFPGA) continue;
          if ( data[0] == 0xbb && data[1] == 0xaa ) break;
          hitNumber ++;
          myTree->Fill();
        }
      }
      else if ( data[0] == 0xbb && data[1] == 0xaa ) std::cout << "No packet header, trailer only ..." << std::endl;
    }
  }
  else { std::cout << "No found input files..." << std::endl; }
  inF.close();

  myTree->Write();
  
  OutputFile->Close();
  delete[] data;
}


void DeBinary()
{
  TString RunNumber = "Run0179";
  int NumOfChips = 6;

  for (int i = 1; i < NumOfChips + 1; i++)
  {
    TString Chip = "0" + std::to_string(i);
    std::cout << "======== Decoding " << Chip << "th " << "chip file =======" << std::endl;
    DoReading(RunNumber, Chip);
    std::cout << "================= Done =================" << std::endl;   
  }    
}
