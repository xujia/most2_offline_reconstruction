#include <iostream>
#include <fstream>

{
  TString inputFile = "./Run0621/Run0621_TbData.root";
  TFile *file = new TFile(inputFile, "read");

  TTree *t1 = (TTree*)file->Get("Event");


  //vector<int>* vec_timeFPGA = new vector<int>();
  //t1->SetBranchAddress("timeFPGA", &vec_timeFPGA);

  vector<int>* vec_row = new vector<int>();
  t1->SetBranchAddress("row", &vec_row);

  vector<int>* vec_col = new vector<int>();
  t1->SetBranchAddress("col", &vec_col);

  int TotEvent = (int)t1->GetEntries();

  std::cout << "TotEvent = " << TotEvent << std::endl;

  ofstream fout("./Run0621/Event.txt");

  int m = 50;
  for (int i = 0; i < m; i++)
  {
    t1->GetEntry(i);
    //if (i >= 2 ) break;
      fout << vec_row->at(3) << " " << vec_row->at(2) << " " << vec_row->at(1) << " " <<vec_row->at(0) << " ";
  }

  fout << endl;

  for (int i = 0; i < m; i++)
  {
    t1->GetEntry(i);

    fout << vec_row->at(3) << " " << vec_col->at(2) << " " << vec_col->at(1) << " " << vec_col->at(0) << " ";
  }

  fout << endl;

  fout.close();


}

