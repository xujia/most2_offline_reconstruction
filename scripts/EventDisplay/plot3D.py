import matplotlib as mpl
from matplotlib import cm
import mpl_toolkits
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt



#fig = plt.figure(figsize=(5, 5), dpi=200)
fig1 = plt.figure(1)
#fig.update_xaxes(autorange="reversed")


#ax = plt.axes(projection='3d')
ax = fig1.add_subplot(111,projection='3d')

#xdata = [1000, 9000, 17000, 25000, 33000]
xdata = [1000, 9000, 17000]
y = np.arange(0, 1023, 1)
z = np.arange(0, 511, 1)
Y, Z = np.meshgrid(y, z)
X1 = 0*Y + xdata[0]
X2 = 0*Y + xdata[1]
X3 = 0*Y + xdata[2]
#X4 = 0*Y + xdata[3]
#X5 = 0*Y + xdata[4]


ax.plot_surface(X1, Y, Z, color = 'blue', alpha = 0.2)
ax.plot_surface(X2, Y, Z, color = 'red', alpha = 0.2)
ax.plot_surface(X3, Y, Z, color = 'green', alpha = 0.2)
#ax.plot_surface(X4, Y, Z, color = 'pink', alpha = 0.2)
#ax.plot_surface(X5, Y, Z, color = 'violet', alpha = 0.2)

file = open("./Run0577/Event.txt", "r")


row = []

'''
for line in file:
    line = line.strip('\n')
    row.append(line.split(' '))

yy = []
zz = []
for i in range(0, 8):
    yy.append(int(row[0][i]))
    zz.append(int(row[1][i]))
    print(yy)
    print(zz)
'''
row = [[283, 318, 230, 47, 23 , 47, 336, 390, 475, 340, 350, 431, 213, 182, 122, 430, 399, 393, 367, 399, 457, 134, 207, 303], [883, 925, 709, 662, 645, 653, 685, 652, 569, 429, 403, 394, 852, 919, 981, 720, 753, 793, 666, 627, 621, 10, 25, 24]]
row = np.array(row)

print(row)

ax.pbaspect = [2.0, 0.6, 0.25]

ax.get_proj = lambda: np.dot(Axes3D.get_proj(ax), np.diag([1.8, 0.7, 0.5, 1.2]))


ydata = []
zdata = []
lineColor = ['b', 'c', 'g', 'k', 'm', 'r', 'violet', 'y', 'teal']


for i in range(0, 7):
    ydata = [int(row[1][3*i]), int(row[1][3*i+1]), int(row[1][3*i+2])]
    zdata = [int(row[0][3*i]), int(row[0][3*i+1]), int(row[0][3*i+2])]
    ax.plot(xdata, ydata, zdata, color = str(lineColor[i]), linestyle = '--', marker = 'x', markersize = 1, linewidth = 0.5)

ax.set_xlabel('beam direction (Z)')
ax.set_ylabel('')
ax.set_zlabel('')
#ax.set_aspect('equal')
#plt.xticks(np.arange(0, 20000, 2000))

ax.set_xlim(0, 25000)
ax.set_ylim(0, 1500)
ax.set_zlim(0, 800)
ax.axes.xaxis.set_ticks([])
ax.axes.yaxis.set_ticks([])
ax.axes.zaxis.set_ticks([])
#ax.spines['left'].set_visible(False)
#ax.axes.get_xaxis().set_visible(False)
#plt.axis('off')

'''
ax.w_xaxis.set_pane_color((1., 1., 1.))
ax.w_yaxis.set_pane_color((1., 1., 1.))
ax.w_zaxis.set_pane_color((1., 1., 1.))
'''
#plt.grid(False)
plt.savefig("3DTrack.pdf")
#plt.show()


plt.figure(2)


#ax = plt.axes(projection='3d')
#plt.

yydata = []
zzdata = []

for i in range(0, 7):
    zzdata = [int(row[0][3*i]), int(row[0][3*i+1]), int(row[0][3*i+2])]
    yydata = [int(row[1][3*i]), int(row[1][3*i+1]), int(row[1][3*i+2])]
    plt.plot(yydata, zzdata, color = lineColor[i], linestyle = '--', marker = 'x', markersize = 3, linewidth = 0.7)
plt.xlim(0, 1023)
#plt.xlable("col")
plt.ylim(0, 511)
#plt.ylable("row")

plt.savefig("Projection.pdf")

plt.show()




