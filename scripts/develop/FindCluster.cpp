#include "clustering/island1.h"



// Get all hits with the same FPGAtimeStamp
void FindCluster(){
    TString RunNumber = "Run0659";

    TFile *InFile = new TFile("MOST2_BSRF_Data/"+RunNumber+"/AllSameTimeHitsLooseFirst4Testt333.root", "read");
    TTree *tInFile = (TTree*)InFile->Get("TimeInfo");

    TFile *OutFile = new TFile("MOST2_BSRF_Data/"+RunNumber+"/Cluster5.root", "RECREATE");

    vector<int>* vec_timeFPGA = new std::vector<int>();
    tInFile->SetBranchAddress("timeFPGA", &vec_timeFPGA);
    vector<int>* vec_timeChip = new std::vector<int>();
    tInFile->SetBranchAddress("timeChip", &vec_timeChip);
    vector<int>* vec_row      = new std::vector<int>();
    tInFile->SetBranchAddress("row", &vec_row);
    vector<int>* vec_col      = new std::vector<int>();
    tInFile->SetBranchAddress("col", &vec_col);
    vector<int>* vec_plane      = new std::vector<int>();
    tInFile->SetBranchAddress("planeID", &vec_plane);

    // Out Tree
    TTree* myTree = new TTree("TrackInfo", "beam data");

    vector<int>* vec_timeFPGAOut   = new std::vector<int>();
    myTree->Branch("timeFPGA", &vec_timeFPGAOut);

    vector<int>* vec_timeChipOut   = new std::vector<int>();
    myTree->Branch("timeChip", &vec_timeChipOut);

    vector<int>* vec_planeOut      = new std::vector<int>();
    myTree->Branch("planeID", &vec_planeOut);

    vector<int>* vec_rowOut        = new std::vector<int>();
    myTree->Branch("row", &vec_rowOut);

    vector<int>* vec_colOut        = new std::vector<int>();
    myTree->Branch("col", &vec_colOut);

    vector<int>* vec_clusterOut    = new std::vector<int>();
    myTree->Branch("clusterID", &vec_clusterOut);

    vector<int>* vec_clusterNum    = new std::vector<int>();
    myTree->Branch("clusterNum", &vec_clusterNum);

    vector<double> Selrow; vector<double> Selcol;
    int TotalHits;
    for (int m = 0; m < (int)tInFile->GetEntries(); m++)
    {
      vec_timeFPGAOut->clear();
      vec_timeChipOut->clear();
      vec_planeOut->clear();
      vec_rowOut->clear();
      vec_colOut->clear();
      vec_clusterOut->clear();
      vec_clusterNum->clear();

      tInFile->GetEntry(m);
      if (m%1000 == 0) {std::cout << "processing " << m << " events" << std::endl;}

      // std::cout << vec_timeFPGA->at(0) << std::endl;

      for (int i = 0; i < vec_timeFPGA->size(); i++)
      {
        vec_timeFPGAOut->push_back(vec_timeFPGA->at(i));
        vec_timeChipOut->push_back(vec_timeChip->at(i));
        vec_planeOut->push_back(vec_plane->at(i));
        vec_rowOut->push_back(vec_row->at(i));
        vec_colOut->push_back(vec_col->at(i));
      }

      TotalHits = vec_timeFPGA->size();
      int aa[TotalHits];
      for (int i = 0; i < TotalHits; i++) aa[i] = 0;

      //std::cout << "TotalHits = " << TotalHits << std::endl;
      int accuPlaneHits = 0;
      for (int i = 0; i < 5; i++)
      {
        Selrow.clear();
        Selcol.clear();
        std::vector<int> Selplane = findVectors(vec_planeOut, i);
        for (int j = Selplane[0]; j < Selplane[Selplane.size() - 1] + 1; j++)
        {
          Selrow.push_back(vec_rowOut->at(j));
          Selcol.push_back(vec_colOut->at(j));
        }
        double size = 0.025;
        for(auto& tx:Selrow){ tx*= size; }
        for(auto& ty:Selcol){ ty*= size; }

        island *t_island = new island(Selrow, Selcol);
        unordered_map<int,vector<pair<double, double>>> islands = t_island->getislands();
        unordered_map<int,vector<int>> islands_id = t_island->getislands_id();

        for(int i = 0; i<t_island->getnumOfislands();i++){
          //cout<<"cluster "<<i<<endl;
          for(int j=0;j<islands[i].size();j++){
            //cout<<"(x,y):"<<int(islands[i][j].first*40+EPSINON)<<","<<int(islands[i][j].second*40+EPSINON)<<" id:"<<islands_id[i][j]<<endl;
            int index = islands_id[i][j];
            //std::cout<<"islands_id[i][j] = " << index << std::endl; //
            //std::cout << "i = " << i << std::endl;
            aa[accuPlaneHits+index] = i;
            vec_clusterNum->push_back(t_island->getnumOfislands());
          }
          // cout<<endl;
        }
        //std::cout << "==================" << std::endl;
        delete t_island;
        //std::cout << "Selrow.size() = " << Selrow.size() << std::endl;
        accuPlaneHits = accuPlaneHits + Selrow.size();

      }
      for (int i = 0; i < TotalHits; i++)
      {
        //std::cout << "aa[i] = " << aa[i] << std::endl;
        vec_clusterOut->push_back(aa[i]);
      }

      myTree->Fill();
    }


  myTree->Write();
  InFile->Close();
  OutFile->Close();

}
