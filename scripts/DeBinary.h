#ifndef _DeBinary_H
#define _DeBinary_H 1

#include <TString.h>
#include <TSystem.h>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>




class DeBinary{

private:

  int valid     = -1;
  int begFlag   = -1;
  int chipID    = 0;
  int dataCount = 0;
  int curID     = 0;

  int preID     = 0;
  int passVal   = 0;
  int row       = 0;
  int col       = 0;
  int timeFPGA  = 0;
  int timeChip  = 0;

  int prerow       = 0;
  int precol       = 0;
  int pretimeFPGA  = 0;
  int pretimeChip  = 0;

  int hitNumber = 0;  // in every packet
  bool IsTrailer = false;
  int num = 0;

  

public:
  DeBinary();
  ~DeBinary();

  void DoReading(TString RunNumber, TString Chip);

};

#endif