bool passTime(int Time1, int Time2)
{
  if (std::abs(Time1 - Time2) <= 1) return true;
  return false;
}

bool passbreak(int Time1, int Time2)
{
  return ((std::abs(Time1 - Time2) > 1) && (Time2 > Time1));
}

bool passcontinue(int Time1, int Time2)
{
  return ((std::abs(Time1 - Time2) > 1) && (Time2 < Time1));
}

// Get the same FPGAtimeStamp
void FindSameTsIn3(){
    TString RunNumber = "BetaData/Run0740";
  
    TFile *file1 = new TFile("./"+RunNumber+"/Outputhit02.root", "read");    // last plane
    TFile *file2 = new TFile("./"+RunNumber+"/Outputhit03.root", "read");    // second plane
    TFile *file3 = new TFile("./"+RunNumber+"/Outputhit04.root", "read");    // first plane

    TFile *OutFile = new TFile("./"+RunNumber+"/SameTimeLooseFirst3.root", "RECREATE");
    TTree *myTree  = new TTree("TimeInfo", "beam data");

    TTree *t1 = (TTree*)file1->Get("HitsInfo");
    TTree *t2 = (TTree*)file2->Get("HitsInfo");
    TTree *t3 = (TTree*)file3->Get("HitsInfo");


    vector<int>* vec_pos = new std::vector<int>();
    myTree->Branch("pointor", &vec_pos);
    vector<int>* vec_timeFPGA = new std::vector<int>();
    myTree->Branch("timeFPGA", &vec_timeFPGA);
    vector<int>* vec_timeChip = new std::vector<int>();
    myTree->Branch("timeChip", &vec_timeChip);
    vector<int>* vec_row      = new std::vector<int>();
    myTree->Branch("row", &vec_row);
    vector<int>* vec_col      = new std::vector<int>();
    myTree->Branch("col", &vec_col);
    vector<int>* vec_plane      = new std::vector<int>();
    myTree->Branch("planeID", &vec_plane);

    int row1, row2, row3, row4, col1, col2, col3, col4, timeFPGA1, timeFPGA2, timeFPGA3, timeFPGA4, timeChip1, timeChip2, timeChip3, timeChip4;

    t1->SetBranchAddress("row", &row1);
    t1->SetBranchAddress("col", &col1);
    t1->SetBranchAddress("timeFPGA", &timeFPGA1);
    t1->SetBranchAddress("timeChip", &timeChip1);

    t2->SetBranchAddress("row", &row2);
    t2->SetBranchAddress("col", &col2);
    t2->SetBranchAddress("timeFPGA", &timeFPGA2);
    t2->SetBranchAddress("timeChip", &timeChip2);

    t3->SetBranchAddress("row", &row3);
    t3->SetBranchAddress("col", &col3);
    t3->SetBranchAddress("timeFPGA", &timeFPGA3);
    t3->SetBranchAddress("timeChip", &timeChip3);




    int TotHitsNum3 = (int)t3->GetEntries();
    int TotHitsNum2 = (int)t2->GetEntries();
    int TotHitsNum1 = (int)t1->GetEntries();


    std::cout << "TotHitsNum3 = " << TotHitsNum3 << std::endl;
    std::cout << "TotHitsNum2 = " << TotHitsNum2 << std::endl;
    std::cout << "TotHitsNum1 = " << TotHitsNum1 << std::endl;

  
    int ktag = 0;
    int jtag = 0;
    int preFPGA1 = 0, preFPGA2 = 0; bool Keepbreak = true;
    for (int i = 0; i < (int)t1->GetEntries(); i++)
    {
      // if (i != 0) preFPGA1 = timeFPGA1;
      t1->GetEntry(i);  
      if (i%10000 == 0) std::cout << "i = " << i << std::endl;
      /* if (Keepbreak == true){
        if (timeFPGA1 < preFPGA1) {
          std::cout << "timeFPGA1 < preFPGA1" << std::endl;
          std::cout << "i" << i <<std::endl;
          std::cout << "timeFPGA1 = " << timeFPGA1 << std::endl;
          std::cout << "preFPGA1 = " << preFPGA1 << std::endl;
          Keepbreak = false;
        }
      } */
      // if (i > 10) break;
      for (int j = jtag; j < (int)t2->GetEntries(); j++)
      { 
        // preFPGA2 = timeFPGA2;
        t2->GetEntry(j);
     /* if (timeFPGA2 < preFPGA2) 
        {
        std::cout << "timeFPGA2 < preFPGA2" << std::endl;
        std::cout << "j" << j <<std::endl;
        std::cout << "timeFPGA2 = " << timeFPGA2 << std::endl;
        std::cout << "preFPGA2 = " << preFPGA2 << std::endl;
        } */
        if (passTime(timeFPGA1, timeFPGA2))
        {
          jtag = j + 1;
          vec_timeFPGA->clear();
          vec_timeChip->clear();
          vec_row->clear();
          vec_col->clear();
          vec_plane->clear();
          vec_pos->clear();    
          int SavetimeFPGA1Times = 0;     
          // std::cout << "passTime(timeFPGA1, timeFPGA2)" << std::endl;
          for (int k = ktag; k < (int)t3->GetEntries(); k++)
          {
            //std::cout << "timeFPGA3 = " << timeFPGA3 << std::endl;
            t3->GetEntry(k);
            if (passTime(timeFPGA2, timeFPGA3))
            {
              ktag = k + 1;
              // std::cout << "passTime(timeFPGA2, timeFPGA3)" << std::endl;
              if ( SavetimeFPGA1Times == 0)
              {
                vec_timeFPGA->push_back(timeFPGA1);
                vec_timeChip->push_back(timeChip1);
                vec_row->push_back(row1);
                vec_col->push_back(col1);
                vec_plane->push_back(2);
                vec_pos->push_back(i);

                vec_timeFPGA->push_back(timeFPGA2);
                vec_timeChip->push_back(timeChip2);
                vec_row->push_back(row2);
                vec_col->push_back(col2);
                vec_plane->push_back(1);
                vec_pos->push_back(j);


                SavetimeFPGA1Times++;
              }
              vec_timeFPGA->push_back(timeFPGA3);
              vec_timeChip->push_back(timeChip3);
              vec_row->push_back(row3);
              vec_col->push_back(col3);
              vec_plane->push_back(0);
              vec_pos->push_back(k);
            }
            else if (passbreak(timeFPGA2, timeFPGA3)) { ktag = k; break;}
            else if (passcontinue(timeFPGA2, timeFPGA3)) { continue; }
          }
          if(vec_timeFPGA->size() > 0)  myTree->Fill();
        }
        else if (passbreak(timeFPGA1, timeFPGA2)) {jtag = j; break;}
        // else if (passbreak(timeFPGA1, timeFPGA2) && Keepbreak) { jtag = j; break; } 
        // else if (passbreak(timeChip1, timeChip2) && !Keepbreak) { std::cout << "aaaa" << std::endl; continue;}
        // else if (passcontinue(timeFPGA1, timeFPGA2)) {std::cout << "continue" << std::endl; continue;}
        else if (passcontinue(timeFPGA1, timeFPGA2) ) {continue;}
      }
    }
  myTree->Write();
}
