#ifndef _CreateCluster_H
#define _CreateCluster_H 1

#include <iostream>
#include <fstream>
#include <vector>

#include <TString.h>
#include <TSystem.h>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

#include "IncludeFun.h"
#include "constants.h"

class CreateCluster{

protected:
  
public:

  CreateCluster();
  ~CreateCluster();
  
  void FormCluster();

private:
};

void pixelToPoint(int row, int col, double Point[2])
{

  if (col <= 1024/2)
  {
    Point[0] = -((511 - col)*25 + 25/2);
  }
  else
  {
    Point[0] = (col - 512)*25 + 25/2;
  }

  if (row  <= 512/2)
  {
    Point[1] = -((255 - row)*25 + 25/2);
  }
  else
  {
    Point[1] = (row - 256)*25 + 25/2;
  }
}

#endif