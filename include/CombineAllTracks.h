#ifndef _CreateAllTracks_H
#define _CreateAllTracks_H 1

#include <iostream>
#include <fstream>
#include <vector>

#include <TString.h>
#include <TSystem.h>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

#include "IncludeFun.h"
#include "constants.h"

class CombineAllTracks{

protected:
  
public:

  CombineAllTracks();
  ~CombineAllTracks();
  
  void CombineCluster();

private:
};

void get_result_in_vector(std::vector<std::vector<int> > & vec,int N,std::vector<int> &tmp, std::vector<std::vector<int>>& tmp_result)
{
    for(int i = 0;i<vec[N].size();++i)
    {
        tmp.push_back(vec[N][i]);
        if (N<vec.size()-1)
        {
            get_result_in_vector(vec,N+1,tmp, tmp_result);
        }
        else
        {
            std::vector<int> one_result;
            for (int i = 0;i<tmp.size();++i)
            {
                one_result.push_back(tmp.at(i));
            }
            tmp_result.push_back(one_result);
        }
        tmp.pop_back();
    }
}

void get_all_combination(std::vector<std::vector<int>>& vec, int result[][6])   // must set num of chips by hand
{
    std::vector<int> tmp_vec;

    std::vector<std::vector<int>> tmp_result; 
    get_result_in_vector(vec, 0, tmp_vec, tmp_result);

    for (int i = 0; i < tmp_result.size(); i++)
    {
        for (int j = 0; j < tmp_result.at(i).size(); j++)
        {
            result[i][j] = tmp_result.at(i).at(j);
        }
    }
}


#endif