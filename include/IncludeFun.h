#ifndef _IncludeFun_H
#define _IncludeFun_H

#include<iostream>
#include<fstream>
#include<vector>

#include <TString.h>
#include <TSystem.h>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>


class IncludeFun{

public:
  IncludeFun();
  ~IncludeFun(){};
  bool passTime(int Time1, int Time2, int boundary);
  bool passbreak(int Time1, int Time2, int boundary);
  bool passcontinue(int Time1, int Time2, int boundary);
  int findItems(std::vector<int>* v, int target);
  std::vector<int> findVectors(std::vector<int>* v, int target);
};


inline bool passTime(int Time1, int Time2, int boundary)
{
  if (std::abs(Time1-Time2) <= boundary) return true;
  return false;
}

inline bool passbreak(int Time1, int Time2, int boundary)
{
  return ((std::abs(Time1 - Time2) > boundary) && (Time2 > Time1));
}

inline bool passcontinue(int Time1, int Time2, int boundary)
{
  return ((std::abs(Time1 - Time2) > boundary) && (Time2 < Time1));
}


inline int findItems(std::vector<int>* v, int target)
{
  int indices;
  for (auto it = v->begin(); it != v->end(); it++) {
    if (*it == target) {
      indices = std::distance(v->begin(), it);
      return indices;
    }
  }
  return -1;
}

inline std::vector<int> findVectors(std::vector<int>* v, int target)
{
  std::vector<int> indices;
  for (auto it = v->begin(); it != v->end(); it++) {
    if (*it == target) {
      indices.push_back(std::distance(v->begin(), it));
    }
  }
  return indices;
}



#endif