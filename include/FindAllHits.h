#ifndef _FindAllHits_H
#define _FindAllHits_H 1

#include <iostream>
#include <fstream>
#include <vector>

#include <TString.h>
#include <TSystem.h>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

#include "IncludeFun.h"
#include "constants.h"

class FindAllHits{

protected:
  
public:

  FindAllHits();
  ~FindAllHits();
  
  void FindHits(std::vector<std::vector<int>> vec_Findpos);

private:
};

int GetCorrectCol(int row, int col)
{
  int newCol;
  if ((row%4 == 0) || (row%4 == 3)){ newCol = col*2; }
  else { newCol = col*2+1; }
  return newCol;
}

int FindTime(TFile *inputFile, int target, std::vector<std::vector<int>*> outputTree, int chipID, int tag[6]){
  TTree *t = (TTree*)inputFile->Get("HitsInfo");
  int row, col, timeFPGA, timeChip;
  t->SetBranchAddress("row", &row);
  t->SetBranchAddress("col", &col);
  t->SetBranchAddress("timeFPGA", &timeFPGA);
  t->SetBranchAddress("timeChip", &timeChip);
  // std::cout << (int)t->GetEntries() << std::endl;
  // std::cout << "chipID = " << chipID << std::endl; std::cout << "tag = " << tag[chipID] << std::endl; std::cout << "=============" << std::endl;
  int FindHitsNum = 0;
  for (int i = tag[chipID]; i < (int)t->GetEntries(); i++)
  {
    t->GetEntry(i);
    if (passTime(target, timeFPGA, 3))
    {
      tag[chipID] = i;
      outputTree[0]->push_back(chipID);
      outputTree[1]->push_back(timeFPGA);
      outputTree[2]->push_back(timeChip);
      outputTree[3]->push_back(row/2);
      outputTree[4]->push_back(GetCorrectCol(row, col));
      FindHitsNum++;
    }
    else if (passbreak(target, timeFPGA, 3)) {tag[chipID] = i; break;}
    else if (passcontinue(target, timeFPGA, 3)) {continue;}
  }
  return FindHitsNum;
}
#endif