## Decode binary file
```
cd scripts
root -l DeBinary.cpp

```
## Track Reconstruction 
### Running 
```
mkdir build
cd build
cmake ../
make
./runReco

```
### Function of code in src/

*  FindTimeStamp: Find matched FPGA time stamp  
*  FindAllHits: Find all hits with same FPGA time stamp  
*  CreateCluster:  Clustering  
*  CombineAllTracks: Combine all possible tracks  


## Track Fit 
### Running
```
cd TrackFitMinuit  
mkdir build  
cd build  
cmake ../  
./trackfit  
```
You can find the output root file in the Output directory



